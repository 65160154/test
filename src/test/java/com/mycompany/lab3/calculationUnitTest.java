/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author ASUS
 */
public class calculationUnitTest {
    
    public calculationUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
     @Test
    public void testCheckwinRow1_0_output_ture(){
        String[][] Table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
       @Test
    public void testCheckwinRow2_0_output_ture(){
        String[][] Table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow3_0_output_ture(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRow3_0_output_fasle(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(false,result);
    }
    @Test
    public void testCheckwinCol0_1_output_ture(){
        String[][] Table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol0_2_output_ture(){
        String[][] Table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinCol0_3_output_ture(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwincol0_3_output_fasle(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"O","O","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(false,result);
    }
    @Test
    public void testCheckwinRowX1_0_output_ture(){
        String[][] Table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckwinRowX2_0_output_ture(){
        String[][] Table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    
    @Test
    public void testCheckwinRowX3_0_output_ture(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinRowX3_0_output_fasle(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(false,result);
    }
    @Test
    public void testCheckwinColX0_1_output_ture(){
        String[][] Table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinColX0_2_output_ture(){
        String[][] Table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwinColX0_3_output_ture(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwincolX0_3_output_fasle(){
        String[][] Table = {{"-","-","-"},{"-","-","-"},{"X","X","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(false,result);
    }
    @Test
    public void testCheckwinColX0_123_output_ture(){
        String[][] Table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwincolX0_321_output_ture(){
        String[][] Table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String CP = "X";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
        @Test
    public void testCheckwinCol0_123_output_ture(){
        String[][] Table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckwincol0_321_output_ture(){
        String[][] Table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String CP = "O";
        boolean result = OX.Checkwin(Table,CP);
        assertEquals(true,result);
    }
    @Test
    public void testCheckdraw_false(){
        String[][] Table = OX.getTable();
        boolean result = OX.checkdraw(Table);
        assertEquals(false,result);
    }
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
